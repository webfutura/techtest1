<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * 1. Create a call which will return all the users which are `active` (users table) and have an Austrian citizenship.
 */
Route::get('/user/active-austrian', function () {
    return User::getActiveByCountry('AT');
});

/**
 * 2. Create a call which will allow you to edit user details just if the user details are there already.
 */
Route::put('user/{user}/set-details', function (Request $request, User $user) {

    // Response when user has no details not specified
    if (!$details = $user->details) {
        return response()->json(['message' => 'User details don\'t exist.']);
    }

    // Don't allow update related user
    $details->update($request->except('user_id'));

    return $details;
});

/**
 * 3. Create a call which will allow you to delete a user just if no user details exist yet.
 */
Route::delete('user/{user}/delete-if-no-details', function (Request $request, User $user) {

    // Response when user has details not specified
    if ($user->details) {
        return response()->json(['message' => 'User details exist.']);
    }

    $user->delete();

    return response()->json(['deleted' => true]);
});
