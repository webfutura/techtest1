<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    // countries: id, name, iso2, iso3
    protected $fillable = ['name', 'iso2', 'iso3'];

    /**
     * Retrieve a country by its iso2 code
     *
     * @param $iso2_code
     * @return Country
     */
    static function findByIso2($iso2_code) {
        return Country::where('iso2', $iso2_code)->first();
    }
}
