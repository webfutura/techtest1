<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    use HasFactory;

    /**
     * Without timestamp
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = ['user_id', 'citizenship_country_id', 'first_name', 'last_name', 'phone_number'];

    /**
     * Get the user country.
     */
    public function country()
    {
        return $this->hasOne(Country::class);
    }

    /**
     * Get the user this details belong to.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
