<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $email
 * @property bool $active
 * @property \App\Models\UserDetails $details
 */
class User extends Model
{
    use HasFactory;

    // users: id, email, active
    protected $fillable = ['email', 'active'];

    /**
     * Get the user details.
     */
    public function details()
    {
        return $this->hasOne(UserDetails::class);
    }

    /**
     * Return active users by country
     *
     * @param $country_iso2
     *
     * @return \App\Models\User[]|\Illuminate\Database\Eloquent\Collection
     */
    static function getActiveByCountry($country_iso2)
    {

        // Return empty collection when country not found
        if (!$country = Country::findByIso2($country_iso2)) {
            return new Collection();
        }

        return User::where('active', 1)
            ->whereRelation('details', 'citizenship_country_id', $country->id)
            ->get();
    }
}
