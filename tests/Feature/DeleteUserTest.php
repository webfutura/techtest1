<?php

namespace Tests\Feature;

use Faker\Factory;
use Tests\TestCase;

use App\Models\User;
use App\Models\UserDetails;

class DeleteUserTest extends TestCase
{

    /**
     * @var int
     */
    protected $userId;

    /**
     * @var \Illuminate\Database\Eloquent\Model[]
     */
    protected $cleanupEntities = [];

    protected $faker;

    function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->cleanupEntities[] = $user = new User([
            'email' => $this->faker->email,
            'active' => $this->faker->boolean,
        ]);
        $user->save();
        $this->userId = $user->id;
    }

    /**
     * Test: A user can be deleted when has no details
     *
     * @return void
     */
    public function test_can_delete_when_no_details()
    {
        $response = $this->delete('/api/user/' . $this->userId . '/delete-if-no-details');

        $response->assertStatus(200)
            ->assertJson(['deleted' => true]);
    }

    /**
     * Test: A user can't be deleted when has details
     *
     * @return void
     */
    public function test_cant_delete_when_has_details()
    {
        $this->cleanupEntities[] = $details = new UserDetails([
            'user_id' => $this->userId,
            'citizenship_country_id' => $this->faker->randomNumber(),
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'phone_number' => $this->faker->phoneNumber,
        ]);
        $details->save();

        $response = $this->delete('/api/user/' . $this->userId . '/delete-if-no-details');

        $response->assertStatus(200)
            ->assertJson(['message' => 'User details exist.']);
    }

    /**
     * Test: 404 when user not found
     *
     * @return void
     */
    public function test_error_when_user_not_found()
    {
        (User::find($this->userId))->delete();

        $response = $this->delete('/api/user/' . $this->userId . '/delete-if-no-details');

        $response->assertStatus(404);
    }

    protected function tearDown(): void
    {

        foreach (array_reverse($this->cleanupEntities) as $entity) {
            if (method_exists($entity, 'delete')) {
                $entity->delete();
            }
        }

        parent::tearDown();
    }


}
